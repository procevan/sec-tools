# Sec Tools

This repository contains cheat sheets and notes prepared by me during my time of learning different security tools

## Viewing The Notes
You need to install [Obsidian](https://obsidian.md/) in order to view the notes properly (although markdown interpreter show the notes media files and links between the notes are not presented in it).

### Steps to follow
* Clone this repository.
* Open Obsidian
* Go to 'Open another vault'>'Open folder as vault'
* Select the directory cloned.
